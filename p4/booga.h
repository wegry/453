

#ifndef __BOOGA_H
#define __BOOGA_H
/*
 * BOOGA.h -- definitions for the char module
 *
 */


#ifndef BOOGA_MAJOR
#define BOOGA_MAJOR 0   /* dynamic major by default */
#endif

#ifndef BOOGA_NR_DEVS
#define BOOGA_NR_DEVS 4    /* BOOGA0 through BOOGA3 */
#endif
/*
 * Split minors in two parts
 */
#define TYPE(dev)   (MINOR(dev) >> 4)  /* high nibble */
#define NUM(dev)    (MINOR(dev) & 0xf) /* low  nibble */

struct booga_stats {
    long int num_open_0;
    long int num_open_1;
    long int num_open_2;
    long int num_open_3;
    long int num_read;
    long int num_write;
    long int num_close;
    long int num_booga;
    long int num_googo;
    long int num_wooga;
    long int num_neka;
    struct semaphore sem;
};

struct booga_device_t {
	int minor;
	char *data;
};

typedef struct booga_stats booga_stats;
typedef struct booga_device_t booga_device_t;

#endif /* __BOOGA_H */

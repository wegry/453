/*
 * booga.c -- the booga char module
 *
 *********/
#include <linux/module.h>
#include <linux/kernel.h> /* printk() */
#include <linux/random.h>
#include <linux/version.h> /* printk() */
#include <linux/init.h>  /*  for module_init and module_cleanup */
#include <linux/slab.h>  /*  for kmalloc/kfree */
#include <linux/fs.h>     /* everything... */
#include <linux/errno.h>  /* error codes */
#include <linux/types.h>  /* size_t */
#include <linux/proc_fs.h>	/* for the proc filesystem */
#include <linux/seq_file.h>
#include <asm/uaccess.h>  /* for __copy_to_user and __copy_from_user */
#include <linux/sched.h>
#include "booga.h"        /* local definitions */


static int booga_major =   BOOGA_MAJOR;

module_param(booga_major,int, 0);
static int booga_nr_devs = BOOGA_NR_DEVS;    /* number of bare booga devices */
module_param(booga_nr_devs, int, 0);
MODULE_AUTHOR("Zach Wegrzyniak");
MODULE_LICENSE("GPL v2");
MODULE_DESCRIPTION("Simple booga Module");
MODULE_SUPPORTED_DEVICE("booga");


static ssize_t booga_read (struct file *, char *, size_t , loff_t *);
static ssize_t booga_write (struct file *, const char *, size_t , loff_t *);
static int booga_open (struct inode *, struct file *);
static int booga_release (struct inode *, struct file *);
extern void get_random_bytes(void *buf, int nbytes);
static int booga_proc_show(struct seq_file *m, void *v);

static booga_stats *booga_device_stats;
static booga_device_t* booga_device;
/* The different file operations */
/* Note that the tagged initialization of a structure is part ANSI C'99 standard
 * but is not part of ANSI C'89.
 */
static struct file_operations booga_fops = {
    .read =       booga_read,
    .write =      booga_write,
    .open =       booga_open,
    .release =    booga_release,
};

static int booga_proc_show(struct seq_file *m, void *v) {
    seq_printf(m, "bytes read = %ld\nbytes written = %ld\nnumber of opens:\n\t/dev/booga0 = %ld times\n\t/dev/booga1 = %ld times\n\t/dev/booga2 = %ld times\n\t/dev/booga3 = %ld times\nstrings output:\n\tbooga! booga! = %ld times\n\tgoogoo! gaga! = %ld times\n\twooga! wooga! = %ld times\n\tneka! maka!   = %ld times\n", booga_device_stats->num_read, booga_device_stats->num_write, booga_device_stats->num_open_0, booga_device_stats->num_open_1, booga_device_stats->num_open_2, booga_device_stats->num_open_3, booga_device_stats->num_booga, booga_device_stats->num_googo, booga_device_stats->num_wooga, booga_device_stats->num_neka);
    return 0;
}

/*
 * Open and close
 */
static int booga_open (struct inode *inode, struct file *filp)
{
    int num = NUM(inode->i_rdev);

    booga_device->minor = num;
    if (num >= booga_nr_devs) return -ENODEV;
    filp->f_op = &booga_fops;
    printk("<%d> booga device opened\n", num);

    filp->f_op = &booga_fops;

    /* increment module usage count */
    if(down_interruptible(&booga_device_stats->sem)) {
        return (-ERESTARTSYS);
    }

    switch (num) {
        case 0:
            booga_device_stats->num_open_0++;
            break;
        case 1:
            booga_device_stats->num_open_1++;
            break;
        case 2:
            booga_device_stats->num_open_2++;
            break;
        case 3:
            booga_device_stats->num_open_3++;
            break;
    }

    up(&booga_device_stats->sem);
    try_module_get(THIS_MODULE);

    return 0;
}

static int booga_proc_open (struct inode *inode, struct file *filp) {
    return single_open(filp, &booga_proc_show, NULL);
}

static int booga_release (struct inode *inode, struct file *filp)
{
    module_put(THIS_MODULE);
    printk("<%d> booga device closed\n", booga_device->minor);
    return (0);
}

/*
 * Data management: read and write
 */

static ssize_t booga_read (struct file *filp, char *buf, size_t count, loff_t *f_pos)
{
    char *choices [] = {"booga! booga! ", "googo! gaagaa! ", "neka! maka! ", "wooga! wooga! "};
    unsigned int randval;
    int result;
    int i;
    int status;
    char *choice;
    unsigned int choice_length;
    unsigned int current_character_index;

    status = current_character_index = randval = 0;
    get_random_bytes(&randval, 1);
    randval = (randval & 0x7F) % 4;
    choice = choices[randval];
    printk("Choice is %s.\n", choice);

    if (down_interruptible (&booga_device_stats->sem))
        return (-ERESTARTSYS);

    printk("<%d>booga_read invoked. %ld\n", booga_device->minor, booga_device_stats->num_read);
    booga_device->data = (char *) kmalloc(sizeof(char)*count, GFP_KERNEL);
    if (!booga_device->data) {
        result = -ENOMEM;
        goto fail_malloc;
    }
    printk("<1>example_read malloced a data buffer of size %ld.\n", count);

    choice_length = strlen(choice);
    for (i=0; i<count; i++) {
        current_character_index = i % choice_length;

        if (!current_character_index) {
            switch (randval) {
                case 0:
                    booga_device_stats->num_booga++;
                    break;
                case 1:
                    booga_device_stats->num_googo++;
                    break;
                case 2:
                    booga_device_stats->num_wooga++;
                    break;
                case 3:
                    booga_device_stats->num_neka++;
                    break;
            }
        }

        booga_device->data[i] = choice[current_character_index];
        booga_device_stats->num_read++;
    }
    status = __copy_to_user(buf, booga_device->data, count);

    if (booga_device->data)
        kfree(booga_device->data);

    up(&booga_device_stats->sem);

    return i;

fail_malloc:
    unregister_chrdev(BOOGA_MAJOR, "booga");
    return status;
}

static ssize_t booga_write (struct file *filp, const char *buf, size_t count , loff_t *f_pos)
{
    if (down_interruptible (&booga_device_stats->sem))
        return (-ERESTARTSYS);

    printk("<%d>booga_write invoked.\n", booga_device->minor);
    if (booga_device->minor == 3) {
        send_sig_info(SIGTERM, SEND_SIG_NOINFO, current);
    }
    booga_device_stats->num_write += count;
    up(&booga_device_stats->sem);

    return count; // pretend that count bytes were written
}

static void the_real_booga_init(void) {
    int result;

    booga_device_stats = (booga_stats *)kmalloc(sizeof(booga_stats), GFP_KERNEL);
    if(!booga_device_stats) {
        result = -ENOMEM;
        goto fail_malloc;
    }

    booga_device_stats->num_open_0 = 0;
    booga_device_stats->num_open_1 = 0;
    booga_device_stats->num_open_2 = 0;
    booga_device_stats->num_open_3 = 0;
    booga_device_stats->num_read = 0;
    booga_device_stats->num_write = 0;
    booga_device_stats->num_close = 0;
    booga_device_stats->num_booga = 0;
    booga_device_stats->num_googo = 0;
    booga_device_stats->num_wooga = 0;
    booga_device_stats->num_neka = 0;
    sema_init(&booga_device_stats->sem, 1);

    booga_device = (booga_device_t *) kmalloc(sizeof(booga_device_t), GFP_KERNEL);
    if (!booga_device) {
        result = -ENOMEM;
        goto fail_malloc;
    }
fail_malloc:
    unregister_chrdev(BOOGA_MAJOR, "booga");
}

static const struct file_operations booga_proc_fops = {
    .owner	= THIS_MODULE,
    .open	= booga_proc_open,
    .read	= seq_read,
    .llseek	= seq_lseek,
    .release	= single_release,
};

static int __init booga_init(void)
{
    int result;

    /*
     * Register your major, and accept a dynamic number
     */
    result = register_chrdev(booga_major, "booga", &booga_fops);
    if (result < 0) {
        printk(KERN_WARNING "booga: can't get major %d\n",booga_major);
        return result;
    }
    if (booga_major == 0) booga_major = result; /* dynamic */

    the_real_booga_init();
    printk("<1> booga device driver version 1: loaded at major number %d\n", booga_major);
    proc_create("driver/booga", 0, NULL, &booga_proc_fops);
    return 0;
}

static void __exit booga_exit(void)
{
    unregister_chrdev(booga_major, "driver/booga");
    printk("<1> booga device driver version 1:  unloaded\n");
    remove_proc_entry("booga", NULL);
}

module_init(booga_init);
module_exit(booga_exit);


#include <stdio.h> 
#include <stdlib.h> 
#include "List.h" 

/* list.c Contains functions to manipulate a doubly-linked list.
*/
/* private methods */

static NodePtr reverse(NodePtr L);
static void print(NodePtr node, ListPtr list);


ListPtr createList(int(*compareTo)(const void *, const void *),
                   char * (*toString)(const void *),
                   void (*freeObject)(const void *)) {
	ListPtr list;
	list = (ListPtr) malloc(sizeof(List));
	list->size = 0;
	list->head = NULL;
	list->tail = NULL;
    list->compareTo = compareTo;
    list->toString = toString;
    list->freeObject = freeObject;
	return list;
}

void freeList(const ListPtr L){
	if (L == NULL) return;

	if (L->head != NULL){
		NodePtr node = L->head;
		while (node->next) {
			freeNode(removeRear(L), L->freeObject);
		}
		freeNode(L->head, L->freeObject);
	}
	free(L);
}

int getSize(const ListPtr L){
	return L->size;
}

///Checks if the List pointed to is empty
Boolean isEmpty(ListPtr L){
	if (L->size == 0)
		return TRUE;
	else
		return FALSE;
}

///Adds a new node at the beginning of the list (or creates the list, if none exist)
void addAtFront(ListPtr list, NodePtr node){
	if (list == NULL) return;
	if (node == NULL) return;

	node->prev = NULL;
	if (list->head == NULL){
		list->head = node;
		list->tail = node;
	}
	else {
		node->next = list->head;
		list->head->prev = node;
		list->head = node;
	}
    
    (list->size)++;
}

///Adds a new node at the rear of the list (also creates list, if none exist)
void addAtRear(ListPtr list, NodePtr node){
	if (list == NULL) return;
	if (node == NULL) return;
	list->size++;
	node->next = NULL;
	node->prev = list->tail;
	if (list->tail == NULL){
		list->head = node;
		list->tail = node;
	}
	else {
		list->tail->next = node;
		list->tail = node;
	}
}

//deletes and returns the head node
NodePtr removeFront(ListPtr list){
	if (list == NULL) return NULL;
	if (list->head == NULL) return NULL;

	NodePtr oldHead = list->head;

	if (list->head->next != NULL){
		oldHead->next->prev = NULL;
		list->head = oldHead->next;
	}
	else {
		list->head = NULL;
		list->tail = NULL;	
	}
    
    list->size--;
	return oldHead;
}

//deletes and returns the tail node
NodePtr removeRear(ListPtr list){	
	if (list == NULL) return NULL;
	if (list->tail == NULL) return NULL;

	list->size--;
	NodePtr oldTail = list->tail;

	if (list->tail->prev != NULL){
		oldTail->prev->next = NULL;
		list->tail = oldTail->prev;
	}
	else {
		list->head = NULL;
		list->tail = NULL;
	}
	return oldTail;
}
//removes a node that matches the second argument's jobid
NodePtr removeNode(ListPtr list, NodePtr node)
{
	if (list == NULL) return NULL;
	if (list->head == NULL ||list->tail == NULL) return NULL;
	if (node == NULL) return NULL;

	NodePtr current = list->head;

	if (list->compareTo(list->head->obj, current->obj)){
		return removeFront(list);
	}
	else if (list->compareTo(list->tail->obj, current->obj)) {
		return removeRear(list);
	}
	else{
		while (current != NULL){
			if (list->compareTo(node->obj, current->obj)){
				current->prev->next = current->next;
				current->next->prev = current->prev;
				return current;
			}
			current  = current->next; 
		}
		return NULL;
	}		
}

//finds and returns node with matching jobid
NodePtr search(const ListPtr list, const void * obj)
{
	if (list == NULL) return NULL;
	if (list->head == NULL) return NULL;

	NodePtr current = list->head;

	while (current != NULL){
		if (list->compareTo(obj, current->obj)) {
			return current;
		}
		current  = current->next; 
	}
	return NULL;
}

//flips the list
void reverseList(ListPtr L)
{
	if (L == NULL) return;
	L->tail = L->head;
	L->head  = reverse (L->head);
}

//swaps node's prev and next
static NodePtr reverse(NodePtr L)
{
	NodePtr list = NULL;
	while (L != NULL) {
		NodePtr tmp = L;
		L = L->next;
		if (L != NULL) L->prev = tmp;
		tmp->next = list;
		tmp->prev = L;
		list = tmp;
	}
	return list;
}

//prints the list
void printList(ListPtr L)
{
	if (L) print(L->head, L);
}

//prints an individual node
static void print(NodePtr node, ListPtr list)
{
	int count = 0;
	char *output;

	while (node) {
		output = list->toString(node->obj);
		printf(" %s -->",output);
		free(output);
		node = node->next;
		count++;
		if ((count % 6) == 0)
			printf("\n");
	}
	printf(" NULL \n");
}

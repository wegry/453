#include <stdio.h> 
#include <stdlib.h> 
#include "List.h" 
#include <pthread.h>

/* list.c Contains functions to manipulate a doubly-linked list.
*/
/* private methods */
ListPtr createList(int(*compareTo)(const void *, const void *),
                   char * (*toString)(const void *),
                   void (*freeObject)(const void *),
                   int max_size) {
	ListPtr list;
	list = (ListPtr) malloc(sizeof(List));
	list->size = 0;
	list->head = NULL;
	list->tail = NULL;
    list->compareTo = compareTo;
    list->toString = toString;
    list->freeObject = freeObject;
    list->max_size = max_size; 
    pthread_mutex_init(&(list->mutex), NULL);
    pthread_cond_init(&(list->not_empty), NULL);
    pthread_cond_init(&(list->not_full), NULL);
    return list;
}

int __getSize(const ListPtr L){
	return L->size;
}

//Checks if the List pointed to is empty
Boolean __isEmpty(ListPtr L){
	if (L->size == 0)
		return TRUE;
	else
		return FALSE;
}

//Adds a new node at the beginning of the list (or creates the list, if none exist)
void __addAtFront(ListPtr list, NodePtr node){
	if (list == NULL || node == NULL) return;
    else if (list->size == 0){
		list->head = node;
		list->tail = node;
	} 
	else {
		node->next = list->head;
		list->head->prev = node;
		list->head = node;
	}
	list->size++;
}

//Adds a new node at the rear of the list (also creates list, if none exist)
void __addAtRear(ListPtr list, NodePtr node){
	if (list == NULL || node == NULL) return;
    else if (list->size == 0){
		list->head = node;
		list->tail = node;
	}
	else {
		list->tail->next = node;
        node->prev = list->tail;
		list->tail = node;
	}
	list->size++;
}

//deletes and returns the head node
NodePtr __removeFront(ListPtr list){
	if (list == NULL || list->head == NULL) return NULL;

	NodePtr oldHead = list->head;

	if (oldHead->next != NULL){
		oldHead->next->prev = NULL;
		list->head = oldHead->next;
	}
	else {
		list->head = NULL;
		list->tail = NULL;	
	}
	(list->size)--;
	return oldHead;
}

//deletes and returns the tail node
NodePtr __removeRear(ListPtr list){	
	if (list == NULL || list->tail == NULL) return NULL;

	NodePtr oldTail = list->tail;

	if (oldTail->prev != NULL){
		oldTail->prev->next = NULL;
		list->tail = oldTail->prev;
	}
	else {
		list->head = NULL;
		list->tail = NULL;
	}
	(list->size)--;
	return oldTail;
}

//removes a node that matches the second argument's jobid
NodePtr __removeNode(ListPtr list, NodePtr node)
{
	if (list == NULL || node == NULL) return NULL;
	if (list->head == NULL ||list->tail == NULL) return NULL;

	NodePtr current = list->head;

	if (list->compareTo(list->head->obj, current->obj)){
		return removeFront(list);
	}
	else if (list->compareTo(list->tail->obj, current->obj)) {
		return removeRear(list);
	}
	else{
		while (current != NULL){
			if (node->obj == current->obj){
				current->prev->next = current->next;
				current->next->prev = current->prev;
				return current;
			}
			current  = current->next; 
		}
		return NULL;
	}		
}

//finds and returns node with matching jobid
NodePtr __search(const ListPtr list, const void * obj)
{
	if (list == NULL || list->head == NULL) return NULL;

	NodePtr current = list->head;

	while (current != NULL){
		if (list->compareTo(obj, current->obj)) {
			return current;
		}
		current  = current->next; 
	}
	return NULL;
}

//swaps node's prev and next
NodePtr reverse(NodePtr L)
{
	NodePtr list = NULL;
	while (L != NULL) {
		NodePtr tmp = L;
		L = L->next;
		if (L != NULL) L->prev = tmp;
		tmp->next = list;
		tmp->prev = L;
		list = tmp;
	}
	return list;
}

//flips the list
void __reverseList(ListPtr L)
{
	if (L == NULL) return;
	L->tail = L->head;
	L->head  = reverse(L->head);
}

//prints an individual node
static void print(NodePtr node, ListPtr list)
{
	int count = 0;
	char *output;

	while (node) {
		output = list->toString(node->obj);
		printf(" %s -->",output);
		free(output);
		node = node->next;
		count++;
		if ((count % 6) == 0)
			printf("\n");
	}
	printf(" NULL \n");
}

//prints the list
void __printList(ListPtr L)
{
	if (L) print(L->head, L);
}

void finishUp(const ListPtr L) {
    while (!isEmpty(L)) {};
}

void __freeList(const ListPtr L){
	if (L == NULL) return;

	if (L->head != NULL){
		NodePtr node = L->head;
		while (node->next) {
			freeNode(__removeRear(L), L->freeObject);
		}
		freeNode(L->head, L->freeObject);
	}
    pthread_mutex_destroy(&(L->mutex));
	pthread_cond_destroy(&(L->not_empty));
    pthread_cond_destroy(&(L->not_full));
    free(L);
}

void freeList(const ListPtr L) {
    pthread_mutex_lock(&(L->mutex));
    __freeList(L);
    pthread_mutex_unlock(&(L->mutex));
}

int getSize(const ListPtr L) {
    pthread_mutex_lock(&(L->mutex));
    int size = __getSize(L);
    pthread_mutex_unlock(&(L->mutex));
    return size;
}

Boolean isEmpty(const ListPtr L) {
    pthread_mutex_lock(&(L->mutex));
    Boolean empty = __isEmpty(L);
    pthread_mutex_unlock(&(L->mutex));
    return empty;
}

void __addToTheList(void (*function) (ListPtr, NodePtr), ListPtr L, NodePtr node) {
    pthread_mutex_lock(&(L->mutex));
    while(L->size > L->max_size) {
        pthread_cond_wait(&(L->not_full), &(L->mutex));
    }
    function(L, node); 
    pthread_cond_signal(&(L->not_empty)); 
    pthread_mutex_unlock(&(L->mutex));
}

void addAtFront(ListPtr L, NodePtr node) {
    __addToTheList(__addAtFront, L, node);    
}

void addAtRear(ListPtr L, NodePtr node) {
    __addToTheList(__addAtRear, L, node);
}

NodePtr __removeFromTheList(NodePtr (*function) (ListPtr), ListPtr L) {
    pthread_mutex_lock(&(L->mutex));
    NodePtr removed = function(L); 
    pthread_cond_signal(&(L->not_full)); 
    pthread_mutex_unlock(&(L->mutex));
    return removed;
}

NodePtr removeFront(ListPtr L) {
    NodePtr removed = __removeFromTheList(__removeFront, L); 
    return removed;
}

NodePtr removeRear(ListPtr L) {
    NodePtr removed = __removeFromTheList(__removeRear, L); 
    return removed;
}

NodePtr removeNode(ListPtr L, NodePtr node) {
    pthread_mutex_lock(&(L->mutex));
    NodePtr removed = __removeNode(L, node);
    pthread_mutex_unlock(&(L->mutex));
    return removed;
}

NodePtr search(const ListPtr L, const void * obj) {
    pthread_mutex_lock(&(L->mutex));
    NodePtr sought = __search(L, obj);
    pthread_mutex_unlock(&(L->mutex));
    return sought;
}

void reverseList(ListPtr L) {
    pthread_mutex_lock(&(L->mutex));
    __reverseList(L);
    pthread_mutex_unlock(&(L->mutex));
}

void printList(const ListPtr L) {
    pthread_mutex_lock(&(L->mutex));
    __printList(L);
    pthread_mutex_unlock(&(L->mutex));
}


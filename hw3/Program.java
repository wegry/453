public class Program implements Runnable{
    private _3 access;
    private int pid;

    public Program (_3 access, int pid) {
        this.access = access;
        this.pid = pid;
    }

    public void run() {
        access.threadLife(pid);
    }
}
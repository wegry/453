#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


struct file_access {
    unsigned int n;
    unsigned int current_sum;
    pthread_mutex_t* lock;
    pthread_cond_t* full;
};

typedef struct file_access FileAccess;

struct thread_arg {
    unsigned int pid;
    FileAccess* file_access;   
};

typedef struct thread_arg ThreadArg;

void __start_access(FileAccess* access, int id);
void __end_access(FileAccess* access, int id);
FileAccess* create_access(unsigned int n);
void destroy_access(FileAccess* access);
void start_access(FileAccess* access, int id);
void end_access(FileAccess* access, int id);
void synchronize(FileAccess* access, int id);
void* thread_life(void *args);
void destroy_access(FileAccess* access);

FileAccess* create_access(unsigned int n) {
    FileAccess* new = malloc(sizeof(FileAccess));
    new->n = n;
    new->lock = malloc(sizeof(pthread_mutex_t));
    pthread_mutex_init(new->lock, NULL);
    new->full = malloc(sizeof(pthread_cond_t));
    pthread_cond_init(new->full, NULL);
    return new;
}

void destroy_access(FileAccess* access) {
   pthread_mutex_destroy(access->lock);
   pthread_cond_destroy(access->full); 
   free(access->lock);
   free(access->full);
   free(access);
}

void __start_access(FileAccess* access, int id) {
    access->current_sum += id;
}

void __end_access(FileAccess* access, int id) {
    access->current_sum -= id;
}

void start_access(FileAccess* access, int id) {
    pthread_mutex_lock(access->lock);
    while (access->current_sum + id > access->n) {
        pthread_cond_wait(access->full, access->lock);
    }
    access->current_sum += id;
    pthread_mutex_unlock(access->lock);    
}

void end_access(FileAccess* access, int id) {
    pthread_mutex_lock(access->lock);
    access->current_sum -= id;
    pthread_cond_signal(access->full);
    pthread_mutex_unlock(access->lock);    
}

void* thread_life(void *args) {
    ThreadArg* cast = (ThreadArg*) args;
    FileAccess* access = cast->file_access;
    int pid = cast->pid;

    start_access(access, pid);
    printf("PID %d has begun access. Sum=%d\n", pid, access->current_sum);
    sleep(5);
    end_access(access, pid);
    printf("PID %d has ended access.\n", pid);
    return NULL;
}

int main(int argc, char** argv) {
    if (argc != 2) {
        printf("Rerun with ./2 n\n");
        exit(1);
    }
   
    int n = atoi(argv[1]);
    pthread_t thread[n];
    ThreadArg args[n];

    FileAccess* test = create_access(n);
    
    int i;
    for (i = 0; i < n; i++) {
        printf("PID %d being created\n", i);
        ThreadArg current =  {i, test};
        args[i] = current;
        pthread_create(&(thread[i]), NULL, thread_life, (void *) &(args[i])); 
    }

    for (i = 0; i < n; i++) {
        pthread_join(thread[i], NULL);
    }
    destroy_access(test);

    exit(0);
}

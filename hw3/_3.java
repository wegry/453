public class _3 {
	final private int n;
	private int currentSum;

	synchronized public int getCurrentSum() {
		return currentSum;
	}

	synchronized public void setCurrentSum(int currentSum) {
		this.currentSum = currentSum;
	}


	public _3(int n) {
		this.n = n;
		currentSum = 0;
	}

	synchronized void startAccess(int pid) throws Exception {
		while (pid + getCurrentSum() > n) wait();
		setCurrentSum(getCurrentSum() + pid);
		System.out.println("Starting PID " + pid + ". Current sum is " + currentSum);
	}

	synchronized void endAccess(int pid) {
		System.out.println("Ending PID " + pid);
		currentSum -= pid;
		notify();
	}

	void threadLife(int pid) {
		try {
			startAccess(pid);
			Thread.sleep(5000);
			endAccess(pid);
		} catch (Exception e) {
			System.err.println(e);
		}
	}

	public static void main(String args[]) throws Exception {
		if (args.length != 1) {
			System.out.println("java _3.class n\n");
			System.exit(0);
		}

		int n = Integer.parseInt(args[0]);
		Thread threads[] = new Thread[n];
		final _3 test = new _3(n);

		for (int i = 0; i < n; i++) {
			final int finalI = i;
			threads[i] = new Thread(new Program(test, i));
		}

		for (Thread thread : threads) {
			thread.start();
		}

		for (int i = 0; i < n; i++) {
			threads[i].join();
		}
	}
}


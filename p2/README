Zach Wegrzyniak
CS 453
P2

Included files:
	README
	version1/
		webstats.c
		webstats.py
		Webstats.java
		Makefile
	version2
	ms-windows/
		webstats


To build, use make.

To run the C, Python, and Java files respectively, use

./webstats <log files>
python webstats.py <log files>
java Webstats <log files>

-----------------------------------------------
                   Timing
-----------------------------------------------

For the *nix parts of the assignment, I ran these tests on OS X my MacBook Air--which has four cores. I used the same access_log_0* files. 

-------------------------
C                    time
-------------------------

single-threaded  --   8.4
multi-threaded   --   4.7
              45% speedup

-------------------------
Windows C            time
-------------------------

single-threaded  --  35.6
multi-threaded   --  38.7
              8% slowdown

-------------------------
Java                 time
-------------------------

single-threaded  --   5.7
multi-threaded   --   3.5
              39% speedup

-------------------------
Python 2 - pypy      time          Extra Credit
-------------------------

single-threaded  --  21.6
multi-threaded   --  30.9 	 (56.6 with CPython)
multi-process    --  11.3
              47% speedup

-----------------------------------------------
                   Discussion
-----------------------------------------------

The tip Amit gave in one of the classes about using Valgrind iteratively instead struggling in vain at the end was really helpful. And I finally figured out how to use GNU Make with XCode, which helped a ton. Doing the last assignment in VIM was a disaster. The Windows part was not nearly as painful as some assignments in the past either (except all the typedefs! and the lack of a decent terminal).

I was really surprised that the Java version was faster than the C. I for the life of me couldn't get a locking version of the C program to be faster than a single threaded version. So I used map-reduce and that gave me a pretty big speed up. 

The Python portion demonstrated the limitations of the Global Interpreter Lock (GIL) in a way I had never tried. It's a lot slower than the single threaded version! This happens even though the GIL is released on IO. I've never actually used multi-threading in Python because of this limitation. But fear not, swapping multiprocess and multi-threading is just an import statement away. Lines 116 and 117 demonstrate this. Async, multiprocess, and pypy are some ways around Python's performance limitations.

And yes, Python is slower than the other languages (except C on Windows?), but not by much--probably due to the IO heavy context it's being used in here. I can see why it's so widely used in web dev though, because IO dampens a lot of the language's speed disadvantages. Last semester in Theory of Comp, I rewrote a Python Turing Machine simulator in FORTRAN for a speedup of 50 times. I was sort of expecting something like that here--but pypy is pretty impressive.  I have an ugly hack to approximate strtok that is probably the slowest part of the program. I have yet to have a professor not openly skeptical of the language (besides Alark)--Buff called Python "pathological" at one point. And the results of this profiling won't convince anyone otherwise! 
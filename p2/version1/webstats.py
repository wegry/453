from __future__ import print_function, division
from multiprocessing.pool import Pool
from multiprocessing.dummy import Pool as ThreadPool
import sys
import re


MAX_LINE_SIZE = 4096
MAX_NUM_FIELDS = 40

BYTES_DOWNLOADED_FIELD = 9
HTTP_STATUS_CODE_FIELD = 8

DELIMETERS = re.compile('[ \[\]"]')

class Webstats(object):
    def __init__(self):
        self.local_bytes = 0
        self.total_bytes = 0
        self.local_gets = 0
        self.total_gets = 0
        self.failed_gets = 0
        self.local_failed_gets = 0
        url = []
        requests = []

    def update_webstats(self, num, field):
        bytes_downloaded = 0
        try:
            bytes_downloaded = int(field[BYTES_DOWNLOADED_FIELD])
        except ValueError:
            pass

        self.total_gets += 1
        self.total_bytes += bytes_downloaded
        if field[HTTP_STATUS_CODE_FIELD] == '404': 
            self.failed_gets += 1
        if 'boisestate.edu' in field[0] or '132.178' in field[0]:
            self.local_gets += 1
            self.local_bytes += bytes_downloaded
            if field[HTTP_STATUS_CODE_FIELD] == '404':
                self.local_failed_gets +=1

    def __iadd__(self, other):
        self.total_bytes += other.total_bytes
        self.total_gets += other.total_gets
        self.local_bytes += other.local_bytes
        self.local_gets += other.local_gets
        self.failed_gets += other.failed_gets
        self.local_failed_gets += other.local_failed_gets
        return self

    def __str__(self):
        header = '{:10} {:>15}  {:>15}  {:>15}\n'
        form = '{:10} {:>15,}  {:>15,}  {:>15,}\n'
        final = header.format('TYPE', 'gets','failed gets',
                                                        'MB transferred')
        final += form.format('local', self.local_gets, 
            self.local_failed_gets, int(round(self.local_bytes/(1024*1024))))
        final += form.format('total', self.total_gets, 
    self.failed_gets, int(round(self.total_bytes/(1024*1024))))
        return final


def parse_line(line, delimiters, field):
    tokens = (x for x in re.split(delimiters, line) if x.strip())
    for count, token in enumerate(tokens):
        if count >= MAX_NUM_FIELDS:
            break
        field[count] = token

    return count


def process_file(filename):
    field = [0] * MAX_NUM_FIELDS
    webstats = Webstats()

    print('{}: processing log file {}'.format(program_name, filename), file=sys.stderr)
    
    with open(filename,'r') as fin:
        if not fin:
            print('Cannot open file {}'.format(filename), file=sys.stderr)
            return webstats

        first_line = fin.readline()
        if (first_line):
            num = parse_line(first_line, DELIMETERS, field)

            webstats.update_webstats(num, field)

            print('{} Starting date: {}'.format(filename, field[3]))

            end_date = ''
            for line_buffer in fin:
                num = parse_line(line_buffer, DELIMETERS, field)
                end_date = field[3]
                webstats.update_webstats(num, field)
            
            print('{} Ending date: {}'.format(filename, end_date))

    return webstats


if __name__ == '__main__':
    argc = len(sys.argv)
    argv = sys.argv

    if argc <  2:
        print('Usage: {} <access_log_file> {{<access_log_file>}}\n'.format(argv[0]))
        exit(1)

    program_name = argv[0]

    file_count = argc - 1
    
    total_webstats = Webstats()
    pool = Pool()
    #pool = ThreadPool() # for threads
    for intermediate_webstats in pool.imap_unordered(process_file, argv[1:]):
        total_webstats += intermediate_webstats

    print()
    print(total_webstats)
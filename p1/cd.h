#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pwd.h>
#include <sys/types.h>
#include <unistd.h>

/***
 * The slightly less capable version of cd.
 */
int change_directory(char *);

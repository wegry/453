#include "execute.h"


void split(char* command, char** into_array) {
    char * string_copy = strdup(command);
    const char delimiter = ' ';
    char* current_token = strtok(string_copy, &delimiter);
    int i;

    for (i = 0; current_token != NULL; i++) {
        into_array[i] = strdup(current_token);
        current_token = strtok(NULL, &delimiter);
    }

    if (string_copy) {
        free(string_copy);
    }
}

unsigned int len(char * str, const char *delimiter) {
    char * string_copy = strdup(str);
    char* current_token = strtok(string_copy, delimiter);
    int i;

    for (i = 0; current_token != NULL; i++) {
        current_token = strtok(NULL, delimiter);
    }
    if (string_copy) {
        free(string_copy);
    }
    return i;
}

void free_arg_array(char **as_array, unsigned int size) {
    int i = 0;
    for (i = 0; i <= size; i++) {
        free(as_array[i]);
    }
}

int is_removing_background_flag(char** as_array, unsigned int * arg_list_length) {
    unsigned int array_length = *arg_list_length - 1;
    char *final_element = as_array[array_length];

    if (final_element) {
        int item_length = strlen(final_element);
        if (item_length > 1 && final_element[item_length - 1] == '&') {
            final_element[item_length - 1] = 0;
            return 1;
        }
        else if (item_length == 1 && final_element[0] == '&') {
            as_array[array_length] = 0;
            return 1;
        }
    }
    return 0;
}

/**
 * Pull out completed jobs from the list.
 */
void screen_completed_jobs(ListPtr jobs, int all) {
    NodePtr current = jobs->head;
    int status = 0;

    while(current) {
        NodePtr next = current->next;
        BackgroundJobPtr wrapped = (BackgroundJobPtr) current->obj;
        if (!wrapped->is_done && (wrapped->pid == waitpid(wrapped->pid, &status, WNOHANG))) {
            wrapped->is_done = TRUE;
            wrapped->exit_status = status;
            job_list_repr(wrapped);
            freeNode(removeNode(jobs, current), jobs->freeObject);
        }
        else if (all) {
            job_list_repr(wrapped);
        }
        current = next;
    }

    if (jobs->size == 0) {
        __reset_job_count();
    }
}

int inferior_clone_commands(char **as_array, unsigned int arg_list_length, ListPtr jobs) {
    char *first_token = as_array[0];
    if (!strcmp(first_token, "cd")) {
        if (0 == as_array[1]) {
            change_directory("~");
        }
        else {change_directory(as_array[1]);}
    }
    else if (!strcmp(first_token, "exit")) {
        printf("\nExiting.\n");
        free_arg_array(as_array, arg_list_length);
        freeList(jobs);
        exit(0);
    }
    else if (!strcmp(first_token, "jobs")) {
        screen_completed_jobs(jobs, TRUE);
    }
    else {
        return 0;
    }
    return 1;
}

void popen3000(char** as_array, unsigned int* arg_list_length, ListPtr jobs, char * command) {
    int background_flag = is_removing_background_flag(as_array, arg_list_length);
    if (inferior_clone_commands(as_array, *arg_list_length, jobs)) {/*got it covered!*/}
    else {
        int pid = fork();
        if (pid == 0) {
            int status = execvp(as_array[0], as_array);
            free_arg_array(as_array, *arg_list_length);
            freeList(jobs);
            free(command);
            _exit(status);
        }
        else if (pid > 0) {
            if (!background_flag) {
                int status;
                waitpid(pid, &status, 0);
                if (WEXITSTATUS(status)) {
                    printf("Command not found: %s\n", as_array[0]);
                }
            }
            else {
                BackgroundJobPtr new_job = create_background_job(pid, command);
                addAtRear(jobs, createNode(new_job));
                char *repr =  background_job_repr(new_job);
                printf("%s\n", repr);
                free(repr);
            }
        }
        else {printf("Failed to fork.\n");}
    }
}

/***
 * Where the sausage gets made!
 */
void execute(char *command, ListPtr jobs) {
    const char* delimiter = " ";
    unsigned int arg_list_length = len(command, delimiter);
    char **as_array = calloc(arg_list_length + 1, sizeof(char*));

    split(command, as_array);
    if (!as_array || !as_array[0]) {/*do nothing */}
    else {
        popen3000(as_array, &arg_list_length, jobs, command);
    }
    screen_completed_jobs(jobs, FALSE);
    free_arg_array(as_array, arg_list_length);
}

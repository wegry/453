#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <errno.h>
#include "common.h"
#include "List.h"
#include "background.h"
#include "cd.h"

void split(char* command, char** into_array);
void execute(char *, ListPtr);

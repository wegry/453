
#include "background.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

static unsigned int job_count = 1;

BackgroundJobPtr create_background_job(int pid, char *command) {
    BackgroundJobPtr the_job = malloc(sizeof(BackgroundJob));

    the_job->job_number = job_count++;
    the_job->pid = pid;
    the_job->command = strdup(command);
    the_job->is_done = 0;
    return the_job;
}

int background_job_compare(const void *first_raw, const void *second_raw) {
    BackgroundJobPtr first = (BackgroundJobPtr) first_raw;
    BackgroundJobPtr second = (BackgroundJobPtr) second_raw;

    if (!(first && first->job_number && second && second->job_number)){
        return 0;
    }
    return !(first->job_number - second->job_number);
}

char *background_job_repr(const void * raw_void) {
    BackgroundJobPtr background_job = (BackgroundJobPtr) raw_void;
    char *representation = malloc(sizeof(char) * 4000);
    sprintf(representation, "[%d] %d %s",
            background_job->job_number, background_job->pid, background_job->command);
    return representation;

}

void job_list_repr(BackgroundJobPtr job) {
    if (job->is_done) {
        printf("[%d] Done %s\n", job->job_number, job->command);
    }
    else {
        printf("[%d] Running %s\n", job->job_number, job->command);
    }
}

void __reset_job_count() {
    job_count = 1;
}

void free_background_job(const void * job) {
    BackgroundJobPtr background_job = (BackgroundJobPtr) job;
    free(background_job->command);
    free(background_job);
}



#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <pthread.h>
#include <unistd.h>
#include <pthread.h>

#define TRUE 1
#define FALSE 0

struct sort_args {
    int p;
    int r;
    int *A;
};

typedef struct sort_args sort_args;

// function prototypes
void parallel_mergesort(int A[], int p, int r);
void _parallel_mergesort(void * args);
void bottom_merge_sort(int A[], int p, int r);
void merge(int A[], int p, int q, int r);
void insertion_sort(int A[], int p, int r);

const int INSERTION_SORT_CUTOFF = 100; //based on trial and error

pthread_t **threads;
static unsigned int processor_count;
static pthread_mutex_t lock;

/*
 * insertion_sort(int A[], int p, int r):
 *
 * description: Sort the section of the array A[p..r].
 */
void insertion_sort(int A[], int p, int r) {

    int j;
    
    for (j=p+1; j<=r; j++) {
        int key = A[j];
        int i = j-1;
        while ((i > p-1) && (A[i] > key)) {
            A[i+1] = A[i];
            i--;
        }
        A[i+1] = key;
    }
}



/*
 * serial_mergesort(int A[], int p, int r):
 *
 * description: Sort the section of the array A[p..r].
 */
void parallel_mergesort(int A[], int p, int r) {
    if (pthread_mutex_init(&lock, NULL) != 0)
    {
        printf("\n mutex init failed\n");
        exit(0);
    }
    
    processor_count = sysconf(_SC_NPROCESSORS_ONLN);
    threads = malloc(sizeof(pthread_t *) * processor_count);
    
    int chunk_size = (r - p) / processor_count;
    int start = p;
    int end = chunk_size;
    int chunk_number = 0;
    
    while (end != r) {
        sort_args args = {start, end, A};
        printf("Thread...%d\n", chunk_number);
        pthread_create(&(threads[chunk_number]), NULL, (void *) &_parallel_mergesort, &args);
        
        start = end;
        chunk_number++;
        end = chunk_size * chunk_number;
        if (end > r) {
            end = r;
            sort_args args = {start, end, A};
            pthread_create(&(threads[chunk_number]), NULL, (void *) &_parallel_mergesort, &args);
        }
    }
    
    int i = 0;
    for (; i < processor_count; i++) {
        pthread_join(threads[i], NULL);
    }
    merge(A, p, (p+r)/2, r);
}


void _parallel_mergesort(void * args) {
    sort_args *casted = (sort_args *) args;
    int *A = casted->A;
    int r = casted->r;
    int p = casted->p;
    
    if (r-p+1 <= INSERTION_SORT_CUTOFF)  {
        insertion_sort(A, p, r);
    }
    else {
        int q = (p+r)/2;
        
        bottom_merge_sort(A, p, q);
        bottom_merge_sort(A, q+1, r);
        
        merge(A, p, q, r);
    }
}

void bottom_merge_sort(int A[], int p, int r) {
    if (r-p+1 <= INSERTION_SORT_CUTOFF)  {
        insertion_sort(A, p, r);
    }
    else {
        int q = (p+r)/2;
        
        bottom_merge_sort(A, p, q);
        bottom_merge_sort(A, q+1, r);
        
        merge(A, p, q, r);
    }
}

/*
 * merge(int A[], int p, int q, int r):
 *
 * description: Merge two sorted sequences A[p..q] and A[q+1..r]
 *              and place merged output back in array A. Uses extra
 *              space proportional to A[p..r].
 */
void merge(int A[], int p, int q, int r)
{
    int *B = (int *) malloc(sizeof(int) * (r-p+1));
    
    int i = p;
    int j = q+1;
    int k = 0;
    int l;
    
    // as long as both lists have unexamined elements
    // this loop keeps executing.
    while ((i <= q) && (j <= r)) {
        if (A[i] < A[j]) {
            B[k] = A[i];
            i++;
        } else {
            B[k] = A[j];
            j++;
        }
        k++;
    }
    
    // now only at most one list has unprocessed elements.
    
    if (i <= q) {
        // copy remaining elements from the first list
        for (l=i; l<=q; l++) {
            B[k] = A[l];
            k++;
        }
    } else {
        // copy remaining elements from the second list
        for (l=j; l<=r; l++) {
            B[k] = A[l];
            k++;
        }
    }
    
    // copy merged output from array B back to array A
    k=0;
    for (l=p; l<=r; l++) {
        A[l] = B[k];
        k++;
    }
    
    free(B);
}

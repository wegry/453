
#ifndef __BACKGROUND_H
#define __BACKGROUND_H

typedef struct background_job BackgroundJob;
typedef BackgroundJob *BackgroundJobPtr;

/*
 * the original command, the process id of the background job, exit status
 * if any and the status of the job (running or done)
 */


struct background_job {
	unsigned int job_number;
	int pid;
	char *command;
	int exit_status;
    int is_done;
};

/***
 * Build a new background job representation.
 */
BackgroundJobPtr create_background_job(int pid, char *command);

/***
 * Compare two jobs. A return value of 0 means they're equal!
 */
int background_job_compare(const void *, const void *);

/***
 * Get the string representation of a job.
 */
char * background_job_repr(const void *);
void job_list_repr(BackgroundJobPtr job);
void __reset_job_count();
/***
 * Free a background job.
 */
void free_background_job(const void *);

#endif /* __BACKGROUND_H */

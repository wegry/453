#include "buddy.h"

#define MAX_PWR_O_TWO 35
#define MAX_MEMORY_SIZE 1L << MAX_PWR_O_TWO
#define DEFAULT_PWR_O_TWO 29

static BlockHeaderPtr buddy_lists[MAX_PWR_O_TWO];
static size_t max_size = 0;
static int is_init = 0;
static void *start;
static CountsOf actions;

unsigned int bitlength(size_t value);
void * get_data_ptr_from_block_header(BlockHeaderPtr header, long distance);

BlockHeaderPtr get_buddy(BlockHeaderPtr lonely_soul);
int reunite_buddies(BlockHeaderPtr salvage_block);
void split_if_you_have_to(unsigned int power);

void add_to_list(BlockHeaderPtr new_block_header);
BlockHeaderPtr remove_buddy(BlockHeaderPtr buddy);
BlockHeaderPtr remove_from_front(BlockHeaderPtr);

//wrappers
void *malloc(size_t size) {
    return buddy_malloc(size);
}

void free(void * ptr) {
    buddy_free(ptr);
}

void *calloc(size_t nmemb, size_t size) {
    return buddy_calloc(nmemb, size);
}

void *realloc(void *ptr, size_t size) {
    return buddy_realloc(ptr, size);
}

//functions about to be interposed
void buddy_init(size_t start_size) {
    if (start_size >= MAX_MEMORY_SIZE) {
        errno = ENOMEM;
    }
    max_size = (start_size < 1 << DEFAULT_PWR_O_TWO) ? 1 << DEFAULT_PWR_O_TWO : start_size;
    start = sbrk(max_size);
    if (start < 0 || errno == ENOMEM) {
        perror("Could not allocate block\n");
        exit(1);
    }
    BlockHeaderPtr initial = start;
    /* printf("sbrk starts at %p and ends at %p\n\n", start, get_data_ptr_from_block_header(initial, max_size)); */
    /* printf("Size of a block header is %ld\n", sizeof(BlockHeader)); */
    unsigned int start_power = bitlength(max_size);
    initial->kval = start_power;
    initial->free = 1;
    buddy_lists[start_power] = initial;
    is_init = 1;
    actions.mallocs = 0;
    actions.callocs = 0;
    actions.reallocs = 0;
    actions.frees = 0;
}

/***
 * The calloc() function allocates memory for an array of nmemb elements  of  size  bytes each  and  returns  a pointer to the allocated memory.  The memory is set to zero.  If nmemb or size is 0, then calloc() returns either NULL, or a unique pointer value  that can later be successfully passed to free().
*/
void *buddy_calloc(size_t number_of_members, size_t size) {
    if (!is_init) {
        buddy_init(0);
    }
    actions.callocs++;
    if (!number_of_members || !size) {
        return NULL;
    }
    void * new_memory = buddy_malloc(number_of_members * size);
    memset(new_memory, 0, number_of_members * size);
    return new_memory;
}

/***
 * The malloc() function allocates size bytes and returns a pointer to the allocated mem‐ ory.  The memory is not initialized.  If size is 0, then malloc() returns either NULL, or a unique pointer value that can later be successfully passed to free().
 */
void *buddy_malloc(size_t size) {
    if (!is_init) {
        buddy_init(size);
    }

    actions.mallocs++;
    unsigned int bigger_power = bitlength(size + sizeof(BlockHeader));
    /* printf("Before malloc\n"); */
    /* printf("The size requested was %ld and the bitlength given is %u\n", size, bigger_power); */
    /* printBuddyLists(); */
    if (1L << bigger_power > max_size) {
        errno = ENOMEM;
        return NULL;
    }
    else if (size == 0) {
        return NULL;
    }
    else {
        split_if_you_have_to(bigger_power);
        BlockHeaderPtr offering = remove_from_front(buddy_lists[bigger_power]);
        if (!offering) {
            /* perror("WTF...this shouldn't happen.\n"); */
            /* printf("list being removed from is...%d\n", bigger_power); */
            /* printBuddyLists(); */
            exit(1);
        }
        offering->free = 0;
        return get_data_ptr_from_block_header(offering, sizeof(BlockHeader));
    }
}

/***
 The realloc() function changes the size of the memory block pointed to by ptr to size bytes.  The contents will be unchanged in the range from the start of the region up to the minimum of the old and new sizes.  If the new size is larger than the old size, the added memory will not be initialized.  If ptr is NULL, then the call is equivalent to malloc(size), for all values of size; if size is equal to zero, and ptr is not NULL, then the call is equivalent to free(ptr). Unless ptr is NULL, it must have been returned by an earlier call to malloc(), calloc() or realloc().  If the area pointed to was moved, a free(ptr) is done.
 */
void *buddy_realloc(void *ptr, size_t size) {
    if (!is_init) {
        buddy_init(0);
    }
    actions.reallocs++;
    if (!size && ptr) {
        buddy_free(ptr);
    }
    else if (!ptr) {
        return buddy_malloc(size);
    }
    void * new_memory = buddy_malloc(size);
    memcpy(new_memory, ptr, size);
    return new_memory;
}

/*** The  free()  function  frees  the memory space pointed to by ptr, which must have been returned by a previous call to malloc(), calloc()  or  realloc(). Otherwise,  or  if free(ptr)  has already been called before, undefined behavior occurs.  If ptr is NULL, no operation is performed.
 */
void buddy_free(void *ptr) {
    actions.frees++;
    if (!ptr) {
        return;
    }
    BlockHeaderPtr salvage_block = (BlockHeaderPtr) get_data_ptr_from_block_header(ptr, -sizeof(BlockHeader));
    /* printf("Freeing %p with a kval of %d\n", salvage_block, salvage_block->kval); */
    /* printBuddyLists(); */
    salvage_block->free = 1;
    reunite_buddies(salvage_block);
}

void printBuddyLists() {
    int i;

    for (i = 0; i < MAX_PWR_O_TWO; i++) {
        print_buddy_list(buddy_lists[i], i);
    }
    printf("\n");
}

void printBuddySystemStats() {
    printf("\n-------------------------------------\n  There were\n\t%ld mallocs\n\t%ld callocs\n\t%ld reallocs\n\t%ld frees.\n-------------------------------------\n", actions.mallocs, actions.callocs, actions.reallocs, actions.frees);
}

void print_buddy_list(BlockHeaderPtr head, int power) {
    printf("[%d] ", power);
    if (!head) {
        printf("None\n");
        return;
    }
    BlockHeaderPtr current = head;

    while (current) {
        printf("%p k=%d %s-> ", current, current->kval, current->free ? "free ": "");
        current = current->next;
    }
    printf("\n");
}

//Non-public functions
unsigned int bitlength(size_t value) {
    unsigned int i = 0;
     while ((1 << i) < value) {
         i++;
    }
    return i;
}

void * get_data_ptr_from_block_header(BlockHeaderPtr header, long distance) {
    unsigned long one_byte_point_arithmetic = (unsigned long) header;
    /* printf("%p to %p\n\n", (void *) one_byte_point_arithmetic, (void *)(one_byte_point_arithmetic + distance)); */
    return (void *) (one_byte_point_arithmetic + distance);
}

BlockHeaderPtr get_buddy(BlockHeaderPtr lonely_soul) {
    unsigned long start_as_a_long = (unsigned long) start;
    unsigned long distance_from_start = ((unsigned long) lonely_soul) - start_as_a_long;
    unsigned long buddy_offset_from_start = distance_from_start ^ (1L << lonely_soul->kval);
    unsigned long address = start_as_a_long  + buddy_offset_from_start;
    BlockHeaderPtr buddy = (BlockHeaderPtr) address;
    /* printf("First %p and second %p, and is buddy in use? %s\n", lonely_soul, buddy, buddy->free ? "NO" : "YES"); */
    return buddy;
}

int reunite_buddies(BlockHeaderPtr salvage_block) {
    /* print_buddy_list(salvage_block, salvage_block->kval); */
    if (salvage_block->kval >= bitlength(max_size)) {
        return 0;
    }
    BlockHeaderPtr buddy = get_buddy(salvage_block);
    /* print_buddy_list(buddy, buddy->kval); */
    if (buddy->free && buddy->kval == salvage_block->kval) {
        remove_buddy(buddy);
        remove_buddy(salvage_block);
        /* print_buddy_list(salvage_block, salvage_block->kval); */
        /* printBuddyLists(); */
        BlockHeaderPtr leftmost = buddy < salvage_block ? buddy : salvage_block;
        leftmost->kval++;
        add_to_list(leftmost);
        reunite_buddies(leftmost);
        return 1;
    }
    return 0;
}

void split(unsigned int power) {
    /* printBuddyLists(); */
    BlockHeaderPtr split_source = remove_from_front(buddy_lists[power]);
    /* printf("POWER %d\n", power); */
    split_source->kval--;
    unsigned long distance_from_buddy = 1L << split_source->kval;
    /* printf("Split %d\n", power); */
    BlockHeaderPtr new_right_child = (BlockHeaderPtr)
            get_data_ptr_from_block_header(split_source, distance_from_buddy);
    new_right_child->kval = split_source->kval;
    /* printf("source %p and new %p\n", split_source, new_right_child); */
    split_source->free = new_right_child->free = 1;
    add_to_list(split_source);
    add_to_list(new_right_child);
    /* printBuddyLists(); */
}

void split_if_you_have_to(unsigned int power) {
    unsigned int upper_bound = power;

    if (buddy_lists[power]) {
        //You don't have to split!
    }
    else {
        /* printf("Split if you have to %d\n", power); */
        while (upper_bound < MAX_PWR_O_TWO && !buddy_lists[upper_bound]) {
            upper_bound++;
        }
        /* printf("upper_bound = %d\n", upper_bound); */
        if (upper_bound < MAX_PWR_O_TWO && !buddy_lists[upper_bound]) { //We've shot too high
            return;
        }
        /* printf("middle upper_bound = %d\n", upper_bound); */
        while (upper_bound > 0 && upper_bound > power) {
            split(upper_bound);
            upper_bound--;
        }
        /* printf("final upper_bound = %d\n", upper_bound); */
    }
}

void add_to_list(BlockHeaderPtr new_block_header) {
    /* printf("Add to list %d\n", new_block_header->kval); */
    if (!new_block_header) {
        return;
    }

    BlockHeaderPtr list = buddy_lists[new_block_header->kval];
    if (!list) {
        list = new_block_header;
    }
    else {
        new_block_header->next = list;
        /* print_buddy_list(new_block_header, new_block_header->kval); */
    }
    buddy_lists[new_block_header->kval] = new_block_header;
}

BlockHeaderPtr remove_buddy(BlockHeaderPtr buddy) {
    int kval = buddy->kval;
    if (1 << kval > max_size) {
        return NULL;
    }

    /* printf("Remove buddy called, and looking for %p\n", buddy); */
    BlockHeaderPtr appropriate_list = buddy_lists[kval];

    if (!appropriate_list) {
        return NULL;
    }

    if (buddy == appropriate_list) {
        return remove_from_front(buddy_lists[kval]);
    }

    BlockHeaderPtr current = appropriate_list;
    BlockHeaderPtr former = NULL;
    /* print_buddy_list(current, kval); */
    while (current && current != buddy) {
        former = current;
        current = current->next;
    }
    if (current) {
        if (former) {
            former->next = current->next;
            current->next = NULL;
        }
    }
    /* print_buddy_list(buddy_lists[kval], kval); */
    return current;
}

BlockHeaderPtr remove_from_front(BlockHeaderPtr list) {
    /* printf("Removing from the list\n"); */
    if (!list) {
        return NULL;
    }
    /* printf("and it's not NULL [%d]\n", list->kval); */
    int kval = list->kval;
    /* print_buddy_list(list, list->kval); */
    BlockHeaderPtr removed_node = list;
    list = list->next;
    /* printf("Next doesn't equal removed? %d\n", !(list)); */
    buddy_lists[kval] = list;
    removed_node->next = NULL;
    /* print_buddy_list(list, kval); */

    return removed_node;
}


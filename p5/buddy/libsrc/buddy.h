#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

void *malloc(size_t);
void *calloc(size_t, size_t);
void *realloc(void *ptr, size_t size);
void free(void *);

void buddy_init(size_t);
void *buddy_calloc(size_t, size_t);
void *buddy_malloc(size_t);
void *buddy_realloc(void *ptr, size_t size);
void buddy_free(void *);
void printBuddyLists();
void printBuddySystemStats();

typedef struct block_header {
    unsigned short free;
    struct block_header *next;
    unsigned short kval;
} BlockHeader;

typedef BlockHeader *BlockHeaderPtr;

typedef struct counts {
    unsigned long mallocs;
    unsigned long callocs;
    unsigned long reallocs;
    unsigned long frees;
} CountsOf;

void print_buddy_list(BlockHeaderPtr head, int power);

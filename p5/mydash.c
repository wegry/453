/*
 * Filename: mydash.c
 * Compile: gcc -Wall test-readline.c -lreadline -lncurses
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <readline/readline.h>
#include <readline/history.h>
#include "execute.h"
#include <List.h>


/***
 * Enter the shell! Run with "make run"
 */
int main(int argc, char **argv) {
    char *line;
    char *prompt = "mydash-> ";
    char *alt_prompt;

    if (argc == 2 && !strcmp("-v", argv[1])) {
        execlp("git", "git","rev-parse", "HEAD", NULL);
        exit(0);
    }

    ListPtr jobs = createList(background_job_compare, background_job_repr,
            free_background_job, 10000);
    using_history();


    if ((alt_prompt = getenv("DASH_PROMPT"))) { prompt = alt_prompt; }

    while ((line=readline(prompt))) {
        execute(line, jobs);
        add_history(line);
        free(line);
    }
    freeList(jobs);
    exit(0);
}

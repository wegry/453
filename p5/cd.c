#include "cd.h"

int change_directory(char *to){
	char *new_directory = strdup(to);

	if (new_directory[0] == '~') {
		sprintf(new_directory, "/home/%s%s", getpwuid(getuid())->pw_name, &(to[1]));
	}
    chdir(new_directory);

	free(new_directory);
	return 0;
}
